﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform CameraTarget;
    public Transform LookAtTarget;
    private Vector3 m_Offset;

    void Start ()
    {
        m_Offset = transform.position - CameraTarget.transform.position;
    }

    void FixedUpdate ()
    {
        CamFollow ();
    }

    private void CamFollow ()
    {
        transform.position = new Vector3(CameraTarget.position.x, CameraTarget.position.y, CameraTarget.position.z);
        transform.LookAt(LookAtTarget);
    }
}
