﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTarget : MonoBehaviour {

    public Transform Target;

    public float PositionDamping = 2.0f;
    public float RotateDamping = 2.0f;

    private Vector3 m_distance;
    
    private void Start()
    {
        m_distance = transform.position;
    }

    private void FixedUpdate()
    {
        Follow();
    }
    
    public void Follow()
    {
        Vector3 wantedPosition = Target.position + (Target.rotation * m_distance);
        Vector3 currentPosition = Vector3.Lerp(transform.position, wantedPosition, PositionDamping * Time.deltaTime);
        transform.position = currentPosition;

        Quaternion wantedRotation = Quaternion.LookRotation(Target.position - transform.position, Target.up);
    }

}
