﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Movement : MonoBehaviour {

    public Slider Slider;
    public Text SpeedText;
    public GameObject RotationGO;
    public GameObject EngineGO;

    [Space(10)]
    public int CounterForce;
    public int MaxCounter = 10;
    public int AccellerationForce;
    public int RotationSpeed = 50;
    public int MaxVelocity;
    public float ReturnRotationTimer = 3;
    private float m_ElapsedTime;
    private float m_CurrentMaxVelocity;
    private float m_MaxCounterForceReached;
    private int m_RigidbodyVelocityUI;

    private Vector3 m_StartingPos;
    private Rigidbody m_Rigidbody;
    private Quaternion m_StartingRotation;

    private bool m_CanMove;
    private bool m_CanRotate;
    private bool m_StartStop;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_StartingPos = transform.position;
        Slider.maxValue = MaxCounter;

        RotationGO.SetActive(false);
        EngineGO.SetActive(false);

        m_CanMove = true;
        m_CanRotate = true;

        m_StartingRotation = transform.rotation;
    }

    private void Update()
    {
        Slider.value = CounterForce;
        m_RigidbodyVelocityUI = (int)m_Rigidbody.velocity.magnitude;
        SpeedText.text = m_RigidbodyVelocityUI.ToString();

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (!m_StartStop && CounterForce == 0)
            {
                StartCoroutine(ResetRotation());
            }
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            if (!m_StartStop && CounterForce == 0)
            {
                StartCoroutine(ResetVelocity());
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            transform.position = m_StartingPos;
            CounterForce = 0;
            m_Rigidbody.velocity = Vector3.zero;
            m_Rigidbody.angularVelocity = Vector3.zero;
            transform.rotation = new Quaternion(0, 0, 0, 0);
        }

        if (m_CanMove)
        {
            #region Speed
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if (CounterForce < MaxCounter)
                {
                    CounterForce++;
                    if (CounterForce >= m_MaxCounterForceReached)
                    {
                        m_CurrentMaxVelocity = AccellerationForce * CounterForce;
                        m_MaxCounterForceReached = CounterForce;
                    }
                }
                else
                {
                    m_CurrentMaxVelocity = AccellerationForce * CounterForce;
                    m_MaxCounterForceReached = MaxCounter;
                    CounterForce = MaxCounter;
                }
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (CounterForce > 0)
                {
                    CounterForce = 0;
                    m_CurrentMaxVelocity = 0;
                    m_CurrentMaxVelocity = m_Rigidbody.velocity.magnitude;              
                }
                else
                {
                    CounterForce = 0;
                    m_CurrentMaxVelocity = 0;
                }
            }
            #endregion
        }

        if(m_CanRotate)
        {
            #region Rotation
            if (Input.GetKey(KeyCode.W))
            {
                RotationShip(transform.right);
            }
            if (Input.GetKey(KeyCode.S))
            {
                RotationShip(-transform.right);
            }
            if (Input.GetKey(KeyCode.D))
            {
                RotationShip(transform.up);
            }
            if (Input.GetKey(KeyCode.A))
            {
                RotationShip(-transform.up);
            }

            if (Input.GetKey(KeyCode.Q))
            {
                RollShip(transform.forward);
            }
            if (Input.GetKey(KeyCode.E))
            {
                RollShip(-transform.forward);
            }

            #endregion
            MovementShip();
        }
    }

    void MovementShip()
    {
        if (CounterForce > 0)
        {
            m_Rigidbody.velocity += transform.forward * AccellerationForce * CounterForce * Time.deltaTime;

            m_Rigidbody.velocity = Vector3.ClampMagnitude(m_Rigidbody.velocity, m_CurrentMaxVelocity);
        }
    }

    void RotationShip(Vector3 rotation)
    {
        m_Rigidbody.AddTorque(rotation * RotationSpeed * Time.deltaTime);
        m_Rigidbody.maxAngularVelocity = 5;
    }

    void RollShip(Vector3 rotation)
    {
        transform.Rotate (rotation * 15 * Time.deltaTime);
    }

    IEnumerator ResetRotation()
    {
        m_StartStop = true;
        m_CanMove = false;
        m_CanRotate = false;

        while (m_ElapsedTime < ReturnRotationTimer)
        {
            m_ElapsedTime += Time.deltaTime; 

            transform.rotation = Quaternion.Slerp(transform.rotation, new Quaternion(0, transform.rotation.y,0, transform.rotation.w), m_ElapsedTime * Time.deltaTime);
            RotationGO.SetActive(true);

            m_Rigidbody.angularVelocity = Vector3.zero;
            yield return null;
        }

        if(m_ElapsedTime >= ReturnRotationTimer)
        {
            RotationGO.SetActive(false);

            m_ElapsedTime = 0;
            m_CanMove = true;
            m_CanRotate = true;
            m_StartStop = false;
            yield return null;
        }
    }

    IEnumerator ResetVelocity()
    {
        m_StartStop = true;
        m_CanMove = false;
        m_Rigidbody.angularVelocity = Vector3.zero;

        while (m_Rigidbody.velocity.magnitude >= 1)
        {
            m_ElapsedTime += Time.deltaTime;
            m_Rigidbody.velocity -= m_Rigidbody.velocity / 2 * Time.deltaTime;
            EngineGO.SetActive(true);

            yield return null;
        }

        if (m_Rigidbody.velocity.magnitude <= 1f)
        {
            EngineGO.SetActive(false);

            m_MaxCounterForceReached = 0;
            m_ElapsedTime = 0;
            m_CanMove = true;
            m_StartStop = false;
            yield return null;
        }
    }
}
